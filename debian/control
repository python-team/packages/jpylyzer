Source: jpylyzer
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Mathieu Malaterre <malat@debian.org>
Build-Depends: debhelper-compat (= 12),
               dh-python,
               lmodern,
               pandoc,
               python3,
               python3-lxml,
               python3-pytest,
               python3-setuptools,
               texlive-fonts-recommended,
               texlive-xetex
Standards-Version: 4.6.0
Homepage: https://github.com/openpreserve/jpylyzer
Vcs-Browser: https://salsa.debian.org/python-team/packages/jpylyzer
Vcs-Git: https://salsa.debian.org/python-team/packages/jpylyzer.git

Package: python3-jpylyzer
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: JP2 (JPEG 2000 Part 1) validator and properties extractor
 Validator and feature extractor for JP2 (JPEG 2000 Part 1 - ISO/IEC 15444-1)
 images. Jpylyzer was specifically created to check that a JP2 file really
 conforms to the format's specifications. Additionally jpylyzer is able to
 extract the technical characteristics of each image.

Package: python-jpylyzer-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: JP2 (JPEG 2000 Part 1) validator and properties extractor - doc
 Validator and feature extractor for JP2 (JPEG 2000 Part 1 - ISO/IEC 15444-1)
 images. Jpylyzer was specifically created to check that a JP2 file really
 conforms to the format's specifications. Additionally jpylyzer is able to
 extract the technical characteristics of each image.
 .
 This is the documentation package for jpylyzer
